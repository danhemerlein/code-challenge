const Router = require('koa-router'),
router = new Router(),
model = require('./rhinoceros');
const Joi = require('joi');

// get endangered
router.get('/rhinoceros/endangered', (ctx) => {
  const result = model.getEndangered()
  ctx.response.body = { result }
})

// get one rhino by ID
router.get('/rhinoceros/id=:id', (ctx) => {
  const rhino = model.findRhino(ctx.params.id)
  ctx.response.body = { rhino }
})

// get all rhinos or get by name or species
router.get('/rhinoceros/:nameorspec?', (ctx, next) => {
  const param = ctx.params.nameorspec;

  if (param) {
    const result = model.getByParam(param);
    ctx.response.body = { result };
  } else {
    const rhinoceroses = model.getAll();
    ctx.response.body = { rhinoceroses };
  }
});

router.post('/rhinoceros', (ctx, next) => {

  const schema = Joi.object().keys({
    name: Joi.string().min(1).max(20).required(),
    species: Joi.string().valid(['white_rhinoceros', 'black_rhinoceros',
                              'indian_rhinoceros', 'javan_rhinoceros',
                              'sumatran_rhinoceros']).required()
  });

  const result = Joi.validate({ name: ctx.request.body.name, species: ctx.request.body.species }, schema);

  // joi considers extra keys to be invalid by default

  if (result.error) {
    console.log(result.error.message);
  } else {
    ctx.response.body = model.newRhinoceros(result.value);
  }
});

module.exports = router;
