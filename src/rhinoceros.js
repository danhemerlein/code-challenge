const uuidv4 = require( 'uuid/v4' );
let rhinoceroses = require('./data');

const species = rhinoceroses.map(rhino => rhino.species);
const names = rhinoceroses.map(rhino => rhino.name);

getBySpeciesOrName = (target, arr) => {
  const s = [];
  const result = [];

  for (let i = 0; i < arr.length; i++) {
    if (arr[i] === target) {
      s.push(i);
    }
  }

  for (let i of s) {
    result.push(rhinoceroses[i]);
  }
  return result;
}

exports.getAll = () => {
  return rhinoceroses;
}

exports.newRhinoceros = (data) => {
  const newRhino = {
    id: uuidv4(),
    name: data.name,
    species: data.species
  }
  rhinoceroses.push(newRhino);
  return newRhino;
}

exports.findRhino = (id) => {
  for (let rhino of rhinoceroses) {
    if (rhino.id === id) {
      return rhino;
    }
  }
}

exports.getByParam = param => {
  const spread = [...param]
  // check to see if the param is name or species, species have an underscore
  if (spread.indexOf('_') > -1) {
    return getBySpeciesOrName(param, species)
  } else {
    return getBySpeciesOrName(param, names)
  }
}

exports.getEndangered = () => {
  const dict = {};
  const endangeredSpecies = [];
  const r = [];

  for (let spec of species) {
    if (spec in dict) {
      dict[spec] += 1;
    } else {
      dict[spec] = 1;
    }
  }

  // the dictionary is an object where the keys are the names of the speices and the values are the number of times each occurs in the data set

  for (let spec in dict) {
    if (dict[spec] === 1 || dict[spec] === 2) {
      endangeredSpecies.push(spec);
    }
  }

  endangeredSpecies.map(spec => {
    r.push(getBySpeciesOrName(spec, species));
  })

  return r;
}
